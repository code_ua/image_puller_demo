import axios from 'axios';

const API_URL = process.env.REACT_APP_API_URL;

const PULL_IMAGES_URL = number => `${API_URL}/images/${number}`;


async function pullImages(number) {
  const response = await axios.get(PULL_IMAGES_URL(number));
  return response.data.images;
}

export { pullImages as default };
