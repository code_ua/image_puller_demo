import React from 'react';

import { shallow } from 'enzyme';

import ImagePuller from './index';
import NumberForm from './components/NumberForm';
import ImageItem from './components/ImageItem';


describe('ImagePuller component', () => {
  it('ImagePuller rendering', () => {
    const wrapper = shallow(<ImagePuller />);
    expect(wrapper.find(NumberForm)).toHaveLength(1);
  });

  it('ImagePuller show images', () => {
    const wrapper = shallow(<ImagePuller />);

    wrapper.setState({
      images: Array.from(Array(10), ((_, index) => ({
        id: index,
        src: `${index}`,
      }))),
    });

    expect(wrapper.find(ImageItem)).toHaveLength(10);
  });
});
