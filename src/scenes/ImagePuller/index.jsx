import React from 'react';

import ImageItem from './components/ImageItem';
import NumberForm from './components/NumberForm';

import pullImages from '../../services/api';

import './styles.scss';


class ImagePuller extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      images: null,
      errorMessage: null,
    };
    this.loadImages = this.loadImages.bind(this);
  }

  async loadImages(number) {
    this.setState({ isLoading: true, images: null, errorMessage: null });

    try {
      const images = await pullImages(number);
      this.setState({
        images,
      });
    } catch (e) {
      this.setState({
        errorMessage: e.message,
      });
    }

    this.setState({ isLoading: false });
  }

  render() {
    const { isLoading, images, errorMessage } = this.state;
    return (
      <div className="ImagePuller">
        <NumberForm onSubmit={this.loadImages} />
        {isLoading && <div className="Loader">Loading...</div>}
        {images && (
          <div className="Images">
            {images.map(image => <ImageItem key={image.id} src={image.src} />)}
          </div>
        )}
        {errorMessage && <div className="ErrorMessage">{errorMessage}</div>}
      </div>
    );
  }
}

export default ImagePuller;
