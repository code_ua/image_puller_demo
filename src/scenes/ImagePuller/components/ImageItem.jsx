import React from 'react';
import PropTypes from 'prop-types';

import './ImageItem.scss';


const ImageItem = (props) => {
  const { src, title } = props;
  return (
    <div className="ImageItem">
      <img src={src} alt={title} />
    </div>
  );
};

ImageItem.propTypes = {
  src: PropTypes.string.isRequired,
  title: PropTypes.string,
};

ImageItem.defaultProps = {
  title: '',
};

export default ImageItem;
