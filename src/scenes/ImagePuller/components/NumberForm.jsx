import React from 'react';
import PropTypes from 'prop-types';

import './NumberForm.scss';


class NumberForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMessage: null,
    };

    this.number = React.createRef();
    this.submit = this.submit.bind(this);
  }

  submit(e) {
    e.preventDefault();
    const { value } = this.number.current;
    const { onSubmit } = this.props;
    if (value < 1) {
      this.setState({
        errorMessage: 'Please enter a number > 0',
      });
    } else {
      this.setState({
        errorMessage: null,
      });
      onSubmit(value);
    }
  }

  render() {
    const { errorMessage } = this.state;
    return (
      <form onSubmit={this.submit} className="NumberForm">
        <p className={errorMessage && 'ErrorMessage'}>{errorMessage || 'Please, enter a number:'}</p>
        <input type="number" ref={this.number} />
        <button type="submit">GO</button>
      </form>
    );
  }
}

NumberForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default NumberForm;
