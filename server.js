const express = require('express');

const app = express();

const smiles = [
  'https://emojipedia-us.s3.amazonaws.com/content/2017/09/21/animoji-unicorn-emojipedia.gif',
  'https://emojipedia-us.s3.amazonaws.com/content/2018/02/01/skull-animoji-emojipedia.gif',
  'https://emojipedia-us.s3.amazonaws.com/content/2017/09/21/animoji-rabbit-emojipedia.gif',
  'https://emojipedia-us.s3.amazonaws.com/content/2017/09/21/animoji-poo-emojipedia.gif',
  'https://emojipedia-us.s3.amazonaws.com/content/2017/09/21/animoji-panda-emojipedia.gif',
  'https://emojipedia-us.s3.amazonaws.com/content/2018/01/29/animoji-lion-emojipedia.gif',
  'https://emojipedia-us.s3.amazonaws.com/content/2017/09/21/animoji-fox-emojipedia.gif',
  'https://emojipedia-us.s3.amazonaws.com/content/2018/02/01/dragon-animoji-emojipedia.gif',
  'https://emojipedia-us.s3.amazonaws.com/content/2017/09/21/animoji-chicken-emojipedia.gif',
  'https://emojipedia-us.s3.amazonaws.com/content/2018/02/01/bear-animoji-emojipedia.gif',
  'https://emojipedia-us.s3.amazonaws.com/content/2017/09/21/animoji-alien-emojipedia.gif',
];

app.get('/images/*', (req, res) => {
  const number = parseInt(req.params['0'], 10) || 0;

  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

  if (number < 1) {
    res.statusMessage = 'number should be > 0';
    res.status(500).end();
  } else {
    const images = [];
    for (let i = 0; i < number; i += 1) {
      const smileImage = smiles[Math.floor(Math.random() * Math.floor(smiles.length))];
      images.push({
        id: i,
        src: smileImage,
      });
    }
    const response = {
      number,
      images,
    };
    res.send(JSON.stringify(response));
  }
});

app.listen(4000, () => console.log('Example app listening on port 4000!'));
